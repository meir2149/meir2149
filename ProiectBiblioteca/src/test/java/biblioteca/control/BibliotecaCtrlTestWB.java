package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BibliotecaCtrlTestWB {
    private CartiRepoInterface repo;
    private CartiRepoInterface repo2;
    private BibliotecaCtrl ctrl;
    private BibliotecaCtrl ctrl2;

    private List<String> autori;
    private List<String> cuvinteCheie;

    private Carte c1;
    private Carte c2;

    @Before
    public void setUp() throws Exception {
        repo = new CartiRepo();
        repo2 = new CartiRepo();
        ctrl = new BibliotecaCtrl(repo);
        ctrl2 = new BibliotecaCtrl(repo2);

        autori = new ArrayList<String>() {
            {
                add("autor");
                add("AutorB");
            }
        };

        cuvinteCheie = new ArrayList<String>() {
            {
                add("keywordA");
                add("keywordB");
            }
        };

        c1 = new Carte("Maitreyi", autori, "1", cuvinteCheie);
        c2 = new Carte("Maitreyi2", autori, "2", cuvinteCheie);

        repo.adaugaCarte(c1);
        repo2.adaugaCarte(c1);
        repo2.adaugaCarte(c2);
    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter("test.txt");
        writer.print("");
        writer.close();
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void cautaCarte1() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("String invalid");
        ctrl.cautaCarte("autor1");
    }

    @Test
    public void cautaCarte2() {
        List<Carte> result = new ArrayList<Carte>();
        try {
            result = ctrl.cautaCarte("autor");
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(result.get(0).equals(c1));
    }

    @Test
    public void cautaCarte3() {
        List<Carte> result = new ArrayList<Carte>();
        try {
            result = ctrl2.cautaCarte("autorul");
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(result.isEmpty());
    }
}