package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    private List<String> autori;
    private List<String> cuvinteCheie;
    private Carte c1;
    private Carte c3;
    private Carte c4;
    private Carte c5;
    private Carte c6;
    private Carte c7;
    private Carte c8;
    private Carte c9;

    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;

    @Before
    public void setUp() throws Exception {
        autori = new ArrayList<String>() {
            {
                add("AutorA");
                add("AutorB");
            }
        };

        cuvinteCheie = new ArrayList<String>() {
            {
                add("keywordA");
                add("keywordB");
            }
        };

        repo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(repo);

        c1 = new Carte("Maitreyi", autori, "1", cuvinteCheie);
        c3 = new Carte("", autori, "2010", cuvinteCheie);
        c4 = new Carte("Unu doi trei", autori, "2010", cuvinteCheie);
        c5 = new Carte("Book title", autori, "-1", cuvinteCheie);
        c6 = new Carte("Book title", autori, "an", cuvinteCheie);
        c7 = new Carte("Book2", autori, "2010", cuvinteCheie);
        c8 = new Carte("M No", autori, "1", cuvinteCheie);
        c9 = new Carte("Titlu", autori, "0", cuvinteCheie);
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void adaugaCarteEC1() {
        try {
            ctrl.adaugaCarte(c1);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(repo.getCarti().contains(c1));
    }

    @Test
    public void adaugaCarteEC3() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Titlu invalid!");
        ctrl.adaugaCarte(c3);
    }

    @Test
    public void adaugaCarteEC4() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Titlu invalid!");
        ctrl.adaugaCarte(c4);
    }

    @Test
    public void adaugaCarteEC5() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("An aparitie invalid!");
        ctrl.adaugaCarte(c5);
    }

    @Test
    public void adaugaCarteEC6() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("An aparitie invalid!");
        ctrl.adaugaCarte(c6);
    }

    @Test
    public void adaugaCarteEC7() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Titlu invalid!");
        ctrl.adaugaCarte(c7);
    }

    @Test
    public void adaugaCarteBVA4() {
        try {
            ctrl.adaugaCarte(c8);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(repo.getCarti().contains(c8));
    }

    @Test
    public void adaugaCarteBVA7() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("An aparitie invalid!");
        ctrl.adaugaCarte(c9);
    }
}