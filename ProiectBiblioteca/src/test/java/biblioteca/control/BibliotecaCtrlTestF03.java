package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BibliotecaCtrlTestF03 {
    private CartiRepoInterface repo;
    private BibliotecaCtrl ctrl;

    @Before
    public void setUp() throws Exception {
        repo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(repo);
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void sortCartiValid() {
        List<Carte> sortedBooks = new ArrayList<Carte>();
        try {
            sortedBooks = ctrl.getCartiOrdonateDinAnul("1973");
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertEquals("Poezii", sortedBooks.get(0).getTitlu());
        assertEquals("Mihai Eminescu", sortedBooks.get(0).getReferenti().get(0));
        assertEquals("Poezii", sortedBooks.get(1).getTitlu());
        assertEquals("Sadoveanu", sortedBooks.get(1).getReferenti().get(0));
        assertEquals("Povesti", sortedBooks.get(2).getTitlu());
    }

    @Test
    public void sortCartiNonValid() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("Nu e numar!");
        ctrl.getCartiOrdonateDinAnul("abc");
    }
}