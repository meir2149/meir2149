package biblioteca.begin;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TopDown {
    private Carte c1;
    private List<String> autori;
    private List<String> cuvinteCheie;

    private CartiRepoInterface repo;
    private CartiRepoInterface repoB;
    private CartiRepoInterface repoPA;
    private CartiRepoInterface repoPAB;
    private CartiRepoInterface repoPABC;
    private BibliotecaCtrl ctrl;
    private BibliotecaCtrl ctrlB;
    private BibliotecaCtrl ctrlPA;
    private BibliotecaCtrl ctrlPAB;
    private BibliotecaCtrl ctrlPABC;

    @Before
    public void setUp() throws Exception {
        cuvinteCheie = new ArrayList<String>() {
            {
                add("keywordA");
                add("keywordB");
            }
        };

        autori = new ArrayList<String>() {
            {
                add("AutorA");
                add("AutorB");
            }
        };

        repo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(repo);

        c1 = new Carte("Maitreyi", autori, "1", cuvinteCheie);


        repoB = new CartiRepo();
        ctrlB = new BibliotecaCtrl(repoB);
        repoB.adaugaCarte(c1);

        repoPA = new CartiRepoMock();
        ctrlPA = new BibliotecaCtrl(repoPA);

        repoPAB = new CartiRepoMock();
        ctrlPAB = new BibliotecaCtrl(repoPAB);

        repoPABC = new CartiRepoMock();
        ctrlPABC = new BibliotecaCtrl(repoPABC);
    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter("test.txt");
        writer.print("");
        writer.close();
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testA() {
        try {
            ctrl.adaugaCarte(c1);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(repo.getCarti().contains(c1));
    }

    @Test
    public void testB() throws Exception {
        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("String invalid");
        ctrlB.cautaCarte("autor1");
    }

    @Test
    public void testC() {
        List<Carte> sortedBooks = new ArrayList<Carte>();
        try {
            sortedBooks = ctrl.getCartiOrdonateDinAnul("1973");
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertEquals("Povesti", sortedBooks.get(2).getTitlu());
        assertEquals("Poezii", sortedBooks.get(0).getTitlu());
        assertEquals("Mihai Eminescu", sortedBooks.get(0).getReferenti().get(0));
        assertEquals("Sadoveanu", sortedBooks.get(1).getReferenti().get(0));
        assertEquals("Poezii", sortedBooks.get(1).getTitlu());
    }

    @Test
    public void testPA() {
        try {
            ctrlPA.adaugaCarte(c1);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(repoPA.getCarti().contains(c1));
    }

    @Test
    public void testPAB() throws Exception {
        try {
            ctrlPAB.adaugaCarte(c1);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(repoPAB.getCarti().contains(c1));

        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("String invalid");
        ctrlPAB.cautaCarte("autor1");
    }

    @Test
    public void testPABC() throws Exception {
        try {
            ctrlPABC.adaugaCarte(c1);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertTrue(repoPABC.getCarti().contains(c1));

        exceptionRule.expect(Exception.class);
        exceptionRule.expectMessage("String invalid");
        ctrlPABC.cautaCarte("autor1");

        List<Carte> sortedBooks = new ArrayList<Carte>();
        try {
            sortedBooks = ctrlPABC.getCartiOrdonateDinAnul("1973");
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
        assertEquals("Povesti", sortedBooks.get(2).getTitlu());
        assertEquals("Poezii", sortedBooks.get(0).getTitlu());
        assertEquals("Mihai Eminescu", sortedBooks.get(0).getReferenti().get(0));
        assertEquals("Sadoveanu", sortedBooks.get(1).getReferenti().get(0));
        assertEquals("Poezii", sortedBooks.get(1).getTitlu());
    }
}