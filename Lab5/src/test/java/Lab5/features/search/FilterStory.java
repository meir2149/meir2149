package Lab5.features.search;

import Lab5.steps.serenity.FilterSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class FilterStory {
    @Managed(uniqueSession = true)
    public WebDriver webdriver;
    @Steps
    public FilterSteps user;

    @Test
    public void filterValid() {
        user.is_the_home_page();
        user.filters_brand("Lenovo");
        user.should_see_results("Lenovo");
    }
}
