package Lab5.features.search;

import Lab5.steps.serenity.LoginSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class LoginStory {
    @Managed(uniqueSession = true)
    public WebDriver webdriver;
    @Steps
    public LoginSteps user;

    @Test
    public void loginValid() {
        user.is_the_home_page();
        user.logs_in("andamoga23@gmail.com", "uFQFyv3KKwfReC5");
        user.should_be_logged_in();
    }

    @Test
    public void loginNonValid() {
        user.is_the_home_page();
        user.logs_in("andamoga23@gmail.com", "wrongpass");
        user.should_not_be_logged_in();
    }
}
