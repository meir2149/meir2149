package Lab5.features.search;

import Lab5.steps.serenity.EndUserSteps;
import Lab5.steps.serenity.SearchSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class SearchStory {
    @Managed(uniqueSession = true)
    public WebDriver webdriver;
    @Steps
    public SearchSteps user;

    @Test
    public void searchingByKeywordValid() {
        user.is_the_home_page();
        user.looks_for("laptop");
        user.should_see_definition("laptop");
    }

    @Test
    public void searchingByKeywordNonvalid() {
        user.is_the_home_page();
        user.looks_for("`!");
        user.should_see_message("0 rezultate pentru:");
    }
}
