package Lab5.steps.serenity;

import Lab5.pages.SearchPage;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SearchSteps {
    SearchPage searchPage;

    @Step
    public void is_the_home_page() {
        searchPage.open();
    }

    @Step
    public void enters(String keyword) {
        searchPage.enter_keywords(keyword);
    }

    @Step
    public void should_see_definition(String definition) {
        List<String> items = new ArrayList<>();
        for (int i=0; i<5; i++)
            if(searchPage.getItems().get(i) != null)
                items.add(searchPage.getItems().get(i).toLowerCase());
        assertThat(items, everyItem(containsString(definition)));
    }

    @Step
    public void should_see_message(String definition) {
        String message = searchPage.getMessage();
        assertThat(message, equalTo(definition));
    }

    @Step
    public void starts_search() {
        searchPage.lookup_terms();
    }

    @Step
    public void looks_for(String term) {
        enters(term);
        starts_search();
    }
}
