package Lab5.steps.serenity;

import Lab5.pages.LoginPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class LoginSteps {
    LoginPage loginPage;

    @Step
    public void is_the_home_page() {
        loginPage.open();
    }

    @Step
    public void logs_in(String email, String password) {
        loginPage.login(email, password);
    }

    @Step
    public void should_be_logged_in() {
        assertThat(loginPage.login_successful(), equalTo(true));
    }

    @Step
    public void should_not_be_logged_in() {
        assertThat(loginPage.login_unsuccessful(), equalTo(true));
    }
}
