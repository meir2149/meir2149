package Lab5.steps.serenity;

import Lab5.pages.FilterPage;
import net.thucydides.core.annotations.Step;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.everyItem;

public class FilterSteps {
    FilterPage filterPage;

    @Step
    public void is_the_home_page() {
        filterPage.open();
    }

    @Step
    public void starts_filter(String brand) {
        filterPage.select_option(brand);
    }

    @Step
    public void filters_brand(String brand) {
        starts_filter(brand);
    }

    @Step
    public void should_see_results(String brand) {
        List<String> items = new ArrayList<>();
        for (int i=0; i<5; i++)
            if(filterPage.getItems().get(i) != null)
                items.add(filterPage.getItems().get(i));
        assertThat(items, everyItem(containsString("Lenovo")));
    }
}
