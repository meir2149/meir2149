package Lab5.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("http://www.emag.ro")
public class SearchPage extends PageObject {
    @FindBy(id="searchboxTrigger")
    private WebElementFacade searchTerms;

    @FindBy(id="card_grid")
    private WebElementFacade cardGrid;

    @FindBy(className="searchbox-submit-button")
    private WebElementFacade lookupButton;

    @FindBy(className = "title-phrasing-sm")
    private WebElementFacade message;

    public void enter_keywords(String keyword) {
        searchTerms.type(keyword);
    }

    public void lookup_terms() {
        lookupButton.click();
    }

    public List<String> getItems() {
        return cardGrid.findElements(By.tagName("div")).stream()
                .map( element -> element.getAttribute("data-name") )
                .collect(Collectors.toList());
    }

    public String getMessage() {
        return message.getText();
    }
}
