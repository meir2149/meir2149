package Lab5.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.emag.ro/laptopuri/c?ref=effective_search")
public class FilterPage extends PageObject {
    @FindBy(className = "ph-label")
    private WebElementFacade openSelect;

    @FindBy(className = "ph-option")
    private WebElementFacade option;

    @FindBy(id="card_grid")
    private WebElementFacade cardGrid;


    public void select_option(String brand) {
        openSelect.click();
        WebElement checkbox = option.findElement(By.xpath(".//i"));
        checkbox.click();
    }

    public List<String> getItems() {
        return cardGrid.findElements(org.openqa.selenium.By.tagName("div")).stream()
                .map( element -> element.getAttribute("data-name") )
                .collect(Collectors.toList());
    }
}
