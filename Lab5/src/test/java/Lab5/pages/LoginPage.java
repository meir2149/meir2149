package Lab5.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.emag.ro/user/login?ref=login_widget")
public class LoginPage extends PageObject {
    @FindBy(id = "email")
    WebElementFacade emailInput;

    @FindBy(id = "password")
    WebElementFacade passwordInput;

    @FindBy(className = "auth-submit")
    WebElementFacade continueButton;

    @FindBy(id = "emg-user-menu")
    WebElementFacade userMenu;

    @FindBy(className = "-is-error")
    WebElementFacade errorMessage;

    public void login(String email, String password) {
        emailInput.type(email);
        continueButton.click();
        passwordInput.type(password);
        continueButton.click();
    }

    public boolean login_successful() {
        return userMenu != null;
    }

    public boolean login_unsuccessful() {
        return errorMessage != null;
    }
}
